function _current_dir
  set_color 7197d9
  if _git_in_repo
    if not _git_is_clean
      set_color d27d75
    else if _git_has_untracked_files
      set_color a69450
    else
      set_color 5aa572
    end
  end
  string replace $HOME ' ~' $PWD
end

function _git_in_repo
  command git rev-parse --git-dir > /dev/null 2>&1
end

function _git_is_clean
  command git diff --no-ext-diff --quiet --exit-code
end

function _git_has_untracked_files
  count (command git ls-files --exclude-standard --others) > /dev/null
end

function prompt_character
  if [ $argv = 0 ]
    set_color dfe2eb
  else
    set_color c54040
  end
  printf "•"
end

function fish_prompt
  set last_status $status
  string join ' ' (_current_dir) (prompt_character $last_status) '' | tr -d \n
end
